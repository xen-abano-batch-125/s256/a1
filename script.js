//FIND USERS WITH LETTER 'A' IN THEIR FIRST OR LAST NAME
	db.users.find(
		{
			$or: [{"firstName":{$regex:"a", $options:"i"}},{"lastName":{$regex:"a", $options:"i"}}]
		},
		{
			"firstName":1,
			"lastName":1,
			"_id":0
		}
	);

//FIND USERS WHO ARE ADMINS AND IS ACTIVE
	db.users.find(
		{
			$and: [{"isAdmin":"yes"},{"isActive": "yes"}]
		}
	);

//FIND COURSES WITH LETTER 'U' IN ITS NAME AND HAS A PRICE OF GREATER THAN OR EQUAL TO 13000
	db.courses.find(
		{
			"name":{$regex:"u", $options:"i"},
			"price":{$gte:"13,000"}
		}
	);
